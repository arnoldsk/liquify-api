<?php

require 'functions.php';

// Parameters
$imageUrl = $_GET['url'] ?? null;
$maxWidth = 600;

$circular = isset($_GET['circular']);

try {
    if (!$imageUrl) {
        throw new Exception('Image not found.');
    }

    // Verify the image
    $imageBlob = getVerifiedImageBlobByUrl($imageUrl);

    if (!$imageBlob) {
        throw new Exception('Incorrect image.');
    }

    // Create the base image
    $image = new Imagick();
    $image->readImageBlob($imageBlob);

    // Resize if needed
    if ($image->getImageWidth() > $maxWidth) {
        $image->scaleImage($maxWidth, 0);
    }

    // If given an image with more than one frame, use the first one
    if ($image->getNumberImages() > 0) {
        foreach ($image as $i => $frame) {
            $newImage = clone $frame->getImage();
            $image->clear();
            $image = $newImage;
            break;
        }
    }

    // Set a new format
    $image->setFormat('png');

    $image->swirlImage(rand(0, 1) ? rand(-15, -5) : rand(5, 15));

    // Apply liquid rescaling
    applyLiquidRescaling($image);

    // If circular, cut around the image
    if ($circular) {
        $circleMask = new Imagick(__DIR__ . '/assets/circle-mask.png');
        $circleMask->scaleImage($image->getImageWidth, $image->getImageHeight());

        $image->compositeImage($circleMask, Imagick::COMPOSITE_COPYOPACITY, 0, 0);
    }

    // Output
    header('Content-Type: image/png');
    echo $image->getImageBlob();

    // Clear resources
    $image->clear();
} catch (Exception $e) {
    header('Content-Type: application/json');
    echo json_encode([
        'error' => $e->getMessage(),
    ]);
}
