<?php

if (isset($_SERVER['REQUEST_METHOD'])) {
    die('This script cannot be run from Browser. This is the shell script.');
}

require 'functions.php';

// Parameters
$imageUrl = $argv[1] ?? null;
$exportPath = __DIR__ . '/out.gif';
$framePath = __DIR__ . '/frames/%s.png';
$maxWidth = 600;
$maxFrames = 150;

try {
    if (!$imageUrl) {
        throw new Exception('Image not found.');
    }

    // Verify the image
    $imageBlob = getVerifiedImageBlobByUrl($imageUrl);

    if (!$imageBlob) {
        throw new Exception('Incorrect image.');
    }

    // Create the base image
    $image = new Imagick();
    $image->readImageBlob($imageBlob);
    $imageFrames = $image->coalesceImages();
    $frameCount = count($imageFrames);

    // Validate frame count
    if ($frameCount > $maxFrames) {
        throw new Exception('Maximum of ' . $maxFrames . ' frames allowed!');
    }

    // Create base gif file
    $gif = new Imagick();
    $gif->setFormat('gif');

    // Delete old frame files
    foreach (glob(sprintf($framePath, '*')) as $file) unlink($file);

    // Apply and export all frames
    if ($image->getNumberImages() > 0) {
        foreach ($imageFrames as $i => $frame) {
            // Resize if needed
            if ($frame->getImageWidth() > $maxWidth) {
                $frame->scaleImage($maxWidth, 0);
            }

            // Apply liquid rescaling
            applyLiquidRescaling($frame);//, 0.5, 1, 5);

            $frame->writeImage(sprintf($framePath, $i));

            echo "Progress: " . round($i * 100 / $frameCount) . "%\r";
        }
    }

    // Join and create the gif from local files
    foreach (range(0, $frameCount - 1) as $i) {
        $frameFile = sprintf($framePath, $i);

        $frameImage = new Imagick();
        $frameImage->setFormat('png');
        $frameImage->readImage($frameFile);

        $frameImage->setImageDelay($image->getImageDelay());

        $gif->addImage($frameImage);
    }

    $gif->deconstructImages();

    // Output
    $gif->writeImages($exportPath, true);
    echo 'Image saved: https://arnoldsk.lv/liquify-api/' . basename($exportPath) . PHP_EOL;

    // Clear resources
    $image->clear();
    $gif->clear();
} catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}

