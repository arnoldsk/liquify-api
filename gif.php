<?php

if (isset($_SERVER['REQUEST_METHOD'])) {
    //die('This script cannot be run from Browser. This is the shell script.');
}

require 'functions.php';

// Parameters
//$imageUrl = $argv[1] ?? null;
$imageUrl = $_GET['url'] ?? null;
//$exportPath = __DIR__ . '/out.gif';
$finalGif = new Imagick();
$finalGif->setFormat('gif');
//$framePath = __DIR__ . '/frames/%s.png';
$frameImages = [];
$maxWidth = 600;
$maxFrames = 150;

try {
    if (!$imageUrl) {
        throw new Exception('Image not found.');
    }

    // Verify the image
    $imageBlob = getVerifiedImageBlobByUrl($imageUrl);

    if (!$imageBlob) {
        throw new Exception('Incorrect image.');
    }

    // Create the base image
    $image = new Imagick();
    $image->readImageBlob($imageBlob);
    $imageFrames = $image->coalesceImages();
    $frameCount = count($imageFrames);

    // Validate frame count
    if ($frameCount > $maxFrames) {
        throw new Exception('Maximum of ' . $maxFrames . ' frames allowed!');
    }

    // Create base gif file
    $gif = new Imagick();
    $gif->setFormat('gif');

    // Delete old frame files
    //foreach (glob(sprintf($framePath, '*')) as $file) unlink($file);

    // Apply and store all frames
    if ($image->getNumberImages() > 0) {
        foreach ($imageFrames as $i => $frame) {
            // Resize if needed
            if ($frame->getImageWidth() > $maxWidth) {
                $frame->scaleImage($maxWidth, 0);
            }

            // Apply liquid rescaling
            applyLiquidRescaling($frame);//, 0.5, 1, 5);

            $frameImages[] = $frame;
        }
    }

    // Join and create the gif from frames
    foreach ($frameImages as $frameImage) {
        $frameImage->setFormat('png');
        $frameImage->setImageDelay($image->getImageDelay());

        $finalGif->addImage($frameImage);
    }

    $finalGif->deconstructImages();

    // Output
    header('Content-Type: image/gif');
    echo $finalGif->getImagesBlob();

    // Clear resources
    $image->clear();
    $finalGif->clear();
} catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}

