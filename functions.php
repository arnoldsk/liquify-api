<?php

function isUrl($string) {
    // Quite simple check
    return preg_match('/^https?:\/\/(.+)/', $string) === 1;
}

function getVerifiedImageBlobByUrl($imageUrl) {
    $size = @getimagesize($imageUrl);
    $mimes = [
        'image/gif', 'image/jpeg', 'image/png',
    ];

    if (
        isUrl($imageUrl)
        // Is an array - a successful size fetch
        && is_array($size)
        // Basically check for necessary values
        && count($size) >= 2
        && isset($size['mime'])
        // Correct mime type
        && in_array($size['mime'], $mimes)
        // Less than 4K w/h
        && $size[0] <= 3840
        && $size[1] <= 3840
    ) {
        return file_get_contents($imageUrl);
    }
    return null;
}

function applyLiquidRescaling(Imagick $image, $multiplier = null, $deltaX = null, $rigidity = null) {
    $startWidth = $image->getImageWidth();
    $startHeight = $image->getImageHeight();

    $multiplier = $multiplier > 0 ? $multiplier : 0.5;

    // Set frame options
    $width = $startWidth * $multiplier;
    $height = $startHeight * $multiplier;
    $deltaX = $deltaX >= 0 ? $deltaX : rand(1, 10);
    $rigidity = $rigidity >= 0 ? $rigidity : rand(5, 50);

    // Apply liquid rescaling
    $image->liquidRescaleImage($width, $height, $deltaX, $rigidity);

    // Scale back up
    $image->scaleImage($startWidth * 0.75, $startHeight * 0.75);

    // Reset page size in case it is a gif
    $image->setImagePage($startWidth * 0.75, $startHeight * 0.75, 0, 0);
}
